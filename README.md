<div align="center">

<a href="https://discord.gg/EsUYr3N">
         <img alt="Discord" src="assets/images/conflict1.png">
</a>

</div>

<div align="center">
<a href="https://discord.gg/EsUYr3N">
         <img alt="Discord" src="assets/images/discord-logo.png"
         width=120" height="120">
</a>
        <a href="http://www.byond.com/games/Rod5/Conflict">
         <img alt="BYOND" src="assets/images/byond-logo.png"
         width=100" height="100">
</a>
</div>

# What is Conflict?

[![](http://img.youtube.com/vi/iJ1wV8ItFOU/0.jpg)](http://www.youtube.com/watch?v=iJ1wV8ItFOU "Conflict Final Gameplay trailer / Reveal")


# Setup
Follow these very easy steps to start playing **Conflict** now!


1.  Visit the [**BYOND download page**](http://www.byond.com/download/) and download the latest version. The software is free and safe, and allows you to launch **Conflict**.

2.  Run the installer. After installation, the BYOND pager will automatically open.

3.  Create a BYOND account on the [**BYOND Registration Page**](https://secure.byond.com/Join/). This account is what you will use to log into the BYOND pager, as pictured below.

<img src="/assets/images/setup-1.png"  width="50%">


4.  Join by going to [**Conflict's BYOND Page**](http://www.byond.com/games/Rod5/Conflict) and hit **Play** to launch the game! OR if you know the **Conflict BYOND Server Address**, you can go to the server directly through the BYOND program by selecting **Open...** & Pasting the server information in the field (as depicted below)

<img src="/assets/images/setup-2.png"  width="50%">

*Note that your in-game name can be different from the name of your BYOND key.*

# Make Suggestions, Report Issues & Bugs

**Conflict** is still in active development. The team relies on community feedback and reporting to further improve the game.

If you encounter any issues or bugs or have a suggestion, please report them on the [**Conflict GitLab Issues Page**](https://gitlab.com/Hinashou/conflict/-/issues)

Please use the template provided when you go to make a new issue and apply the following labels in the option below the description so they can be adequately prioritized in the development log.

- Suggestion: ~Suggestion
- Bugs/Issues: ~Bug

To see what is currently proposed, accepted, and in-work, visit the [**Conflict GitLab Development Board.**](https://gitlab.com/Hinashou/conflict/-/boards)

- Accepted Items to be Worked: ~"Development::Accepted"
- Items currently in work: ~"Development::Doing"	

## Team
- Double Hashtag



